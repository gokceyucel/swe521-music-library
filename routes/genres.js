var express = require('express');
var router = express.Router();
var genres = require('../data/genres');

router.get('/', function (req, res, next) {

  // init viewData
  var viewData = {
    'title': 'Genres',
    'route': 'genres'
  };
  var query = req.query.q;

  if (query) {
    genres.getPlainGenresByName(query, function (err, genres) {
      if (err) return next(err);

      viewData.genres = genres;
      res.render('genres', viewData);
    });
  } else {
    genres.getPlainGenres(function (err, genres) {
      if (err) return next(err);

      viewData.genres = genres;
      res.render('genres', viewData);
    });
  }
});

router.post('/', function (req, res, next) {

  var name = req.body.name;

  genres.insertGenre(name, function (err) {
    if (err) console.error(err);
    res.redirect('genres');
  });
});

router.delete('/', function (req, res, next) {

  var ids = req.body.ids;

  genres.deleteGenre(ids, function (err) {
    if (err) console.error(err);
    res.send({'redirect': '/genres'});
  });
});

router.post('/update', function (req, res, next) {

  var genreId = req.body.genreId;
  var name = req.body.genreName;

  genres.updateGenre(genreId, name, function (err) {
    if (err) return next(err);
    res.redirect('/genres');
  });
});

module.exports = router;
