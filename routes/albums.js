/**
 * Created by gkc on 17/05/16.
 */

var express = require('express');
var router = express.Router();
var albums = require('../data/albums');
var artists = require('../data/artists');
var record_types = require('../data/record_types');

var appendExtraContentToViewData = function (viewData, next) {

  // get artists
  artists.getPlainArtists(function (err, artists) {
    if (err) return next(err);
    viewData.artists = artists;

    // get record types
    record_types.getPlainRecordTypes(function (err, record_types) {
      if (err) return next(err);
      viewData.record_types = record_types;
      next(null, viewData);
    });
  });
};

router.get('/', function (req, res, next) {

  // init viewData
  var viewData = {
    'title': 'Albums',
    'route': 'albums'
  };
  var query = req.query.q;

  if (query) {
    // get full albums by name
    albums.getFullAlbumsByName(query, function (err, albums) {
      if (err) return next(err);
      viewData.albums = albums;

      appendExtraContentToViewData(viewData, function (err, viewData) {
        if (err) return next(err);
        res.render('albums', viewData);
      });
    });
  } else {
    // get full albums
    albums.getFullAlbums(function (err, albums) {
      if (err) return next(err);
      viewData.albums = albums;

      appendExtraContentToViewData(viewData, function (err, viewData) {
        if (err) return next(err);
        res.render('albums', viewData);
      });
    });
  }
});

router.get('/artist/:artistId', function (req, res, next) {

  var viewData = {'route': 'albums'};
  var artistId = req.params.artistId;

  albums.getFullAlbumsByArtistId(artistId, function (err, albums) {
    if (err) return next(err);

    viewData.albums = albums;
    viewData.title = albums.length > 0
      ? 'Albums of ' + albums[0].artist.name
      : 'No albums for this artist';

    appendExtraContentToViewData(viewData, function (err, viewData) {
      if (err) return next(err);
      res.render('albums', viewData);
    });
  });
});

router.get('/record_type/:recordTypeId', function (req, res, next) {

  var viewData = {'route': 'albums'};
  var recordTypeId = req.params.recordTypeId;

  albums.getFullAlbumsByRecordTypeId(recordTypeId, function (err, albums) {
    if (err) return next(err);

    viewData.albums = albums;
    viewData.title = albums.length > 0
      ? albums[0].record_type.name + ' albums'
      : 'No albums for this record type';

    appendExtraContentToViewData(viewData, function (err, viewData) {
      if (err) return next(err);
      res.render('albums', viewData);
    });
  });
});

router.post('/', function (req, res, next) {

  var name = req.body.name;
  var year = req.body.year;
  var artist_id = req.body.artist;
  var record_type_id = req.body.record_type;

  albums.insertAlbum(name, year, artist_id, record_type_id, function (err) {
    if (err) console.error(err);
    res.redirect('albums');
  })
});

router.delete('/', function (req, res, next) {

  var ids = req.body.ids;

  albums.deleteAlbum(ids, function (err) {
    if (err) console.error(err);
    res.send({'redirect': '/albums'});
  });
});

router.post('/update', function (req, res, next) {

  var albumId = req.body.albumId;
  var name = req.body.albumName;
  var year = req.body.year;
  var recordTypeId = req.body.recordTypeId;
  var artistId = req.body.artistId;

  albums.updateAlbum(albumId, name, artistId, year, recordTypeId, function (err) {
    if (err) return next(err);
    res.redirect('/albums');
  });
});

module.exports = router;