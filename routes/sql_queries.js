/**
 * Created by gkc on 02/06/16.
 */

var express = require('express');
var router = express.Router();
var db = require('../db').db();

router.get('/', function (req, res, next) {

  // init viewData
  var viewData = {
    'title': 'SQL Queries',
    'route': 'sql_queries'
  };

  res.render('sql_queries', viewData);
});

router.post('/', function (req, res, next) {
  var query = req.body.query;
  if (query) {
    db.query(query, function (err, result) {
      res.send({
        'err': err,
        'result': result
      });
    });
  } else {
    res.send({
      'err': 'Empty query!',
      'result': 'Error'
    });
  }
});

module.exports = router;
