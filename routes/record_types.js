var express = require('express');
var router = express.Router();
var record_types = require('../data/record_types');

router.get('/', function (req, res, next) {

  // init viewData
  var viewData = {
    'title': 'Record Types',
    'route': 'record_types'
  };
  var query = req.query.q;

  if (query) {
    record_types.getPlainRecordTypesByName(query, function (err, record_types) {
      if (err) return next(err);

      viewData.record_types = record_types;
      res.render('record_types', viewData);
    });

  } else {
    record_types.getPlainRecordTypes(function (err, record_types) {
      if (err) return next(err);

      viewData.record_types = record_types;
      res.render('record_types', viewData);
    });
  }
});

router.post('/', function (req, res, next) {

  var name = req.body.name;

  record_types.insertRecordType(name, function (err) {
    if (err) console.error(err);
    res.redirect('record_types');
  });
});

router.delete('/', function (req, res, next) {

  var ids = req.body.ids;

  record_types.deleteRecordTypes(ids, function (err) {
    if (err) console.error(err);
    res.send({'redirect': '/record_types'});
  });
});

router.post('/update', function (req, res, next) {

  var recordTypeId = req.body.recordTypeId;
  var name = req.body.recordTypeName;

  record_types.updateRecordType(recordTypeId, name, function (err) {
    if (err) return next(err);
    res.redirect('/record_types');
  });
});

module.exports = router;
