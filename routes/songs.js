var express = require('express');
var router = express.Router();
var songs = require('../data/songs');
var artists = require('../data/artists');
var albums = require('../data/albums');
var genres = require('../data/genres');

var appendExtraContentToViewData = function (viewData, next) {

  // get artists
  artists.getPlainArtists(function (err, artists) {
    if (err) console.error(err);
    viewData.artists = artists;

    // get albums
    albums.getPlainAlbums(function (err, albums) {
      if (err) console.error(err);
      viewData.albums = albums;

      // get genres
      genres.getPlainGenres(function (err, genres) {
        if (err) console.error(err);
        viewData.genres = genres;
        next(null, viewData);
      });
    });
  });
};

router.get('/', function (req, res, next) {

  // init viewData
  var viewData = {
    'title': 'Songs',
    'route': 'songs'
  };
  var query = req.query.q;

  if (query) {
    // get songs with artist names
    songs.getFullSongsByName(query, function (err, songs) {
      if (err) return next(err);
      viewData.songs = songs;

      appendExtraContentToViewData(viewData, function (err, viewData) {
        if (err) return next(err);
        res.render('songs', viewData);
      });
    });
  } else {
    // get songs with artist names
    songs.getFullSongs(function (err, songs) {
      if (err) return next(err);

      viewData.songs = songs;
      appendExtraContentToViewData(viewData, function (err, viewData) {
        if (err) return next(err);
        res.render('songs', viewData);
      });
    });
  }
});

router.get('/album/:albumId', function (req, res, next) {

  // init viewData
  var viewData = {'route': 'songs'};
  var albumId = req.params.albumId;

  songs.getFullSongsByAlbumId(albumId, function (err, songs) {
    if (err) return next(err);

    viewData.songs = songs;
    viewData.title = songs.length > 0
      ? 'Songs of ' + songs[0].album.name + ' album'
      : 'No songs in this album';
    appendExtraContentToViewData(viewData, function (err, viewData) {
      if (err) return next(err);

      res.render('songs', viewData);
    });
  });
});

router.get('/artist/:artistId', function (req, res, next) {

  // init viewData
  var viewData = {'route': 'songs'};
  var artistId = req.params.artistId;

  songs.getFullSongsByArtistId(artistId, function (err, songs) {
    if (err) return next(err);

    viewData.songs = songs;
    viewData.title = songs.length > 0
      ? 'Songs of ' + songs[0].artist.name
      : 'No songs for this artist';
    appendExtraContentToViewData(viewData, function (err, viewData) {
      if (err) return next(err);

      res.render('songs', viewData);
    });
  });
});

router.get('/genre/:genreId', function (req, res, next) {

  // init viewData
  var viewData = {'route': 'songs'};
  var genreId = req.params.genreId;

  songs.getFullSongsByGenreId(genreId, function (err, songs) {
    if (err) return next(err);

    viewData.songs = songs;
    viewData.title = songs.length > 0
      ? 'Songs of ' + songs[0].genre.name + ' genre'
      : 'No songs for this genre';
    appendExtraContentToViewData(viewData, function (err, viewData) {
      if (err) return next(err);

      res.render('songs', viewData);
    });
  });
});

router.post('/', function (req, res, next) {

  var name = req.body.name;
  var play_time = req.body.play_time;
  var artist_id = req.body.artist;
  var album_id = req.body.album;
  var genre_id = req.body.genre;
  var year = req.body.year;

  songs.insertSong(name, play_time, artist_id, album_id, genre_id, year, function (err) {
    if (err) console.error(err);
    res.redirect('songs');
  });
});

router.delete('/', function (req, res, next) {

  var ids = req.body.ids;

  songs.deleteSongs(ids, function (err) {
    if (err) console.error(err);
    res.send({'redirect': '/songs'});
  });
});

router.post('/update', function (req, res, next) {

  var songId = req.body.songId;
  var name = req.body.songName;
  var year = req.body.year;
  var albumId = req.body.albumId;
  var artistId = req.body.artistId;
  var playTime = req.body.playTime;
  var genreId = req.body.genreId;

  //var updateSong = function (songId, name, playTime, year, artistId, recordTypeId, genreId, next) {
  songs.updateSong(songId, name, playTime, year, artistId, albumId, genreId, function (err) {
    if (err) return next(err);
    res.redirect('/songs');
  });
});

module.exports = router;
