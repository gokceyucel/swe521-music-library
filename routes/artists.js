var express = require('express');
var router = express.Router();
var artists = require('../data/artists');

router.get('/', function (req, res, next) {

  // init viewData
  var viewData = {
    'title': 'Artists',
    'route': 'artists'
  };
  var query = req.query.q;

  if (query) {
    artists.getPlainArtistsByName(query, function (err, artists) {
      if (err) return next(err);
      viewData.artists = artists;
      res.render('artists', viewData);
    });
  } else {
    artists.getPlainArtists(function (err, artists) {
      if (err) return next(err);
      viewData.artists = artists;
      res.render('artists', viewData);
    });
  }
});

router.post('/', function (req, res, next) {

  var name = req.body.name;
  var bio = req.body.bio;

  artists.insertArtist(name, bio, function (err) {
    if (err) console.error(err);
    res.redirect('artists');
  });
});

router.delete('/', function (req, res, next) {

  var ids = req.body.ids;

  artists.deleteArtist(ids, function (err) {
    if (err) console.error(err);
    res.send({'redirect': '/artists'});
  });
});

router.post('/update', function (req, res, next) {

  var artistId = req.body.artistId;
  var name = req.body.artistName;
  var bio = req.body.artistBio;

  artists.updateArtist(artistId, name, bio, function (err) {
    if (err) return next(err);
    res.redirect('/artists');
  });
});

module.exports = router;

