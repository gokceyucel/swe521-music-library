/**
 * Created by gkc on 15/05/16.
 */

var db = require('../db').db();

var fullAlbumsQuery =
  'SELECT     album.id, ' +
  '           album.name album_name, ' +
  '           album.year, ' +
  '           artist.id artist_id, ' +
  '           artist.name artist_name, ' +
  '           rt.id record_type_id, ' +
  '           rt.name record_type_name ' +
  'FROM       albums album ' +
  'INNER JOIN artists artist  ON artist.id  = album.artist_id ' +
  'INNER JOIN record_types rt ON rt.id      = album.record_type_id ';

var parseFullAlbums = function (rawFullAlbums) {
  return rawFullAlbums.map(function (a) {
    return {
      'id': a.id,
      'name': a.album_name,
      'year': a.year,
      'artist': {
        'id': a.artist_id,
        'name': a.artist_name
      },
      'record_type': {
        'id': a.record_type_id,
        'name': a.record_type_name
      }
    };
  });
};

var getFullAlbums = function (next) {
  db.query(fullAlbumsQuery, function (err, contents) {
    if (err) return next(err);
    next(null, parseFullAlbums(contents));
  });
};

var getFullAlbumsByName = function (name, next) {
  var query = fullAlbumsQuery + ' WHERE album.name like "%' + name + '%"';

  db.query(query, function (err, contents) {
    if (err) return next(err);
    next(null, parseFullAlbums(contents));
  });
};

var getFullAlbumsByArtistId = function (artistId, next) {
  var query = fullAlbumsQuery + ' WHERE artist.id = ' + artistId;

  db.query(query, function (err, contents) {
    if (err) return next(err);
    next(null, parseFullAlbums(contents));
  });
};

var getFullAlbumsByRecordTypeId = function (recordTypeId, next) {
  var query = fullAlbumsQuery + ' WHERE rt.id = ' + recordTypeId;

  db.query(query, function (err, contents) {
    if (err) return next(err);
    next(null, parseFullAlbums(contents));
  });
};

var getPlainAlbums = function (next) {
  var query = 'SELECT * FROM albums;';

  db.query(query, function (err, contents) {
    if (err) return next(err);
    next(null, contents);
  });
};

var insertAlbum = function (name, year, artist_id, record_type_id, next) {

  if (name && year && artist_id && record_type_id) {

    db.query(
      'INSERT INTO albums VALUES (null, ' + artist_id + ', "' + name + '", ' + year + ', ' + record_type_id + ')',
      function (err) {
        next(err);
      }
    );
  } else {
    next('Something wrong with insertAlbum params.');
  }
};

var deleteAlbum = function (ids, next) {

  if (ids) {
    db.query("DELETE FROM albums WHERE id IN (" + ids + ")", function (err) {
        next(err);
      }
    );
  } else {
    next('Something wrong with deleteAlbum params.');
  }
};

var updateAlbum = function (albumId, name, artistId, year, recordTypeId, next) {
  var query =
    'UPDATE   albums ' +
    'SET      name="' + name + '", ' +
    '         artist_id=' + artistId + ', ' +
    '         year=' + year + ', ' +
    '         record_type_id=' + recordTypeId + ' ' +
    'WHERE    id=' + albumId + ';';

  db.query(query, function (err) {
    next(err);
  });
};

module.exports = {
  getPlainAlbums: getPlainAlbums,
  getFullAlbums: getFullAlbums,
  getFullAlbumsByName: getFullAlbumsByName,
  getFullAlbumsByArtistId: getFullAlbumsByArtistId,
  getFullAlbumsByRecordTypeId: getFullAlbumsByRecordTypeId,
  insertAlbum: insertAlbum,
  deleteAlbum: deleteAlbum,
  updateAlbum: updateAlbum
};