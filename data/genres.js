/**
 * Created by gkc on 15/05/16.
 */

var db = require('../db').db();

var getPlainGenres = function (next) {
  var query = 'SELECT * FROM genres;';

  db.query(query, function (err, contents) {
    if (err) return next(err);
    next(null, contents);
  });
};

var getPlainGenresByName = function (name, next) {
  var query = 'SELECT * FROM genres WHERE name LIKE "%' + name + '%";';

  db.query(query, function (err, contents) {
    if (err) return next(err);
    next(null, contents);
  });
};

var insertGenre = function (name, next) {

  if (name) {
    db.query(
      'INSERT INTO genres (name) VALUES ("' + name + '")',
      function (err) {
        next(err);
      }
    );
  } else {
    next('Something wrong with insertGenre params.');
  }
};

var deleteGenre = function (ids, next) {

  if (ids) {
    db.query("DELETE FROM genres WHERE id IN (" + ids + ")", function (err) {
        next(err);
      }
    );
  } else {
    next('Something wrong with deleteGenre params.');
  }
};

var updateGenre = function (genreId, name, next) {
  var query =
    'UPDATE   genres ' +
    'SET      name="' + name + '" ' +
    'WHERE    id=' + genreId + ';';

  db.query(query, function (err) {
    next(err);
  });
};

module.exports = {
  getPlainGenres: getPlainGenres,
  getPlainGenresByName: getPlainGenresByName,
  insertGenre: insertGenre,
  deleteGenre: deleteGenre,
  updateGenre: updateGenre
};