/**
 * Created by gkc on 15/05/16.
 */

var db = require('../db').db();

var getPlainSongs = function (next) {
  var query = 'SELECT * FROM songs;';

  db.query(query, function (err, contents) {
    if (err) return next(err);
    next(null, contents);
  });
};

var parseFullSongs = function (rawFullSongs) {
  return rawFullSongs.map(function (s) {
    return {
      'id': s.id,
      'name': s.song_name,
      'year': s.year,
      'play_time': s.play_time,
      'artist': {
        'id': s.artist_id,
        'name': s.artist_name
      },
      'album': {
        'id': s.album_id,
        'name': s.album_name
      },
      'genre': {
        'id': s.genre_id,
        'name': s.genre_name
      }
    };
  });
};

var fullSongsQuery =
  'SELECT     song.id, ' +
  '           song.name song_name, ' +
  '           song.play_time, ' +
  '           song.year, ' +
  '           artist.id artist_id, ' +
  '           artist.name artist_name, ' +
  '           genre.id genre_id, ' +
  '           genre.name genre_name, ' +
  '           album.id album_id, ' +
  '           album.name album_name ' +
  'FROM       songs song ' +
  'INNER JOIN artists artist  ON artist.id  = song.artist_id ' +
  'INNER JOIN genres  genre   ON genre.id   = song.genre_id ' +
  'INNER JOIN albums  album   ON album.id   = song.album_id ';

var getFullSongs = function (next) {
  db.query(fullSongsQuery, function (err, contents) {
    if (err) return next(err);

    var songs = parseFullSongs(contents);
    next(null, songs);
  });
};

var getFullSongsByName = function (name, next) {
  var query = fullSongsQuery + ' WHERE song.name LIKE "%' + name + '%";';

  db.query(query, function (err, contents) {
    if (err) return next(err);

    var songs = parseFullSongs(contents);
    next(null, songs);
  });
};

var getFullSongsByAlbumId = function (albumId, next) {
  var query = fullSongsQuery + ' WHERE album.id = ' + albumId;

  db.query(query, function (err, contents) {
    if (err) return next(err);

    var songs = parseFullSongs(contents);
    next(null, songs);
  });
};

var getFullSongsByArtistId = function (artistId, next) {
  var query = fullSongsQuery + ' WHERE artist.id = ' + artistId;

  db.query(query, function (err, contents) {
    if (err) return next(err);

    var songs = parseFullSongs(contents);
    next(null, songs);
  });
};

var getFullSongsByGenreId = function (genreId, next) {
  var query = fullSongsQuery + ' WHERE genre.id = ' + genreId;

  db.query(query, function (err, contents) {
    if (err) return next(err);

    var songs = parseFullSongs(contents);
    next(null, songs);
  });
};

var deleteSongs = function (ids, next) {

  if (ids) {
    db.query("DELETE FROM songs WHERE id IN (" + ids + ")", function (err) {
        next(err);
      }
    );
  } else {
    next('Something wrong with deleteSongs params.');
  }
};

var insertSong = function (name, play_time, artist_id, album_id, genre_id, year, next) {

  if (name && play_time && artist_id && album_id && genre_id && year) {
    db.query(
      'INSERT INTO songs VALUES (null, "' + name + '", ' + play_time + ', ' + artist_id + ', ' + album_id + ', ' + genre_id + ', ' + year + ')',
      function (err) {
        next(err);
      }
    );
  } else {
    next('Something wrong with insertSong params.');
  }
};

var updateSong = function (songId, name, playTime, year, artistId, albumId, genreId, next) {
  var query =
    'UPDATE   songs ' +
    'SET      name="' + name + '", ' +
    '         artist_id=' + artistId + ', ' +
    '         genre_id=' + genreId + ', ' +
    '         play_time=' + playTime + ', ' +
    '         year=' + year + ', ' +
    '         album_id=' + albumId + ' ' +
    'WHERE    id=' + songId + ';';

  db.query(query, function (err) {
    next(err);
  });
};

module.exports = {
  getPlainSongs: getPlainSongs,
  getFullSongs: getFullSongs,
  getFullSongsByName: getFullSongsByName,
  getFullSongsByAlbumId: getFullSongsByAlbumId,
  getFullSongsByArtistId: getFullSongsByArtistId,
  getFullSongsByGenreId: getFullSongsByGenreId,
  insertSong: insertSong,
  deleteSongs: deleteSongs,
  updateSong: updateSong
};