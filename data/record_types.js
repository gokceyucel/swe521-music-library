/**
 * Created by gkc on 15/05/16.
 */

var db = require('../db').db();

var getPlainRecordTypes = function (next) {
  var query = 'SELECT * FROM record_types;';

  db.query(query, function (err, contents) {
    if (err) return next(err);
    next(null, contents);
  });
};

var getPlainRecordTypesByName = function (name, next) {
  var query = 'SELECT * FROM record_types WHERE name LIKE "%' + name + '%";';

  db.query(query, function (err, contents) {
    if (err) return next(err);
    next(null, contents);
  });
};

var insertRecordType = function (name, next) {

  if (name) {
    db.query(
      'INSERT INTO record_types (name) VALUES ("' + name + '")',
      function (err) {
        next(err);
      }
    );
  } else {
    next('Something wrong with insertRecordType params.');
  }
};

var deleteRecordType = function (ids, next) {

  if (ids) {
    db.query('DELETE FROM record_types WHERE id IN (' + ids + ')', function (err) {
        next(err);
      }
    );
  } else {
    next('Something wrong with deleteRecordType params.');
  }
};

var updateRecordType = function (recordTypeId, name, next) {
  var query =
    'UPDATE   record_types ' +
    'SET      name="' + name + '" ' +
    'WHERE    id=' + recordTypeId + ';';

  db.query(query, function (err) {
    next(err);
  });
};

module.exports = {
  getPlainRecordTypes: getPlainRecordTypes,
  getPlainRecordTypesByName: getPlainRecordTypesByName,
  insertRecordType: insertRecordType,
  deleteRecordTypes: deleteRecordType,
  updateRecordType: updateRecordType
};