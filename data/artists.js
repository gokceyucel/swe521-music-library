/**
 * Created by gkc on 15/05/16.
 */

var db = require('../db').db();

var getPlainArtists = function (next) {
  var query = 'SELECT * FROM artists;';

  db.query(query, function (err, contents) {
    if (err) return next(err);
    next(null, contents);
  });
};

var getPlainArtistsByName = function (name, next) {
  var query = 'SELECT * FROM artists WHERE name LIKE "%' + name + '%";';

  db.query(query, function (err, contents) {
    if (err) return next(err);
    next(null, contents);
  });
};

var insertArtist = function (name, bio, next) {

  if (name) {
    db.query(
      'INSERT INTO artists VALUES (null, "' + name + '", "' + bio + '");',
      function (err) {
        next(err);
      }
    );
  } else {
    next('Something wrong with insertArtist params.');
  }
};

var deleteArtist = function (ids, next) {

  if (ids) {
    db.query("DELETE FROM artists WHERE id IN (" + ids + ")", function (err) {
        next(err);
      }
    );
  } else {
    next('Something wrong with deleteArtist params.');
  }
};

var updateArtist = function (artistId, name, bio, next) {
  var query =
    'UPDATE   artists ' +
    'SET      name="' + name + '", ' +
    '         bio="' + bio + '" ' +
    'WHERE    id=' + artistId + ';';

  db.query(query, function (err) {
    next(err);
  });
};

module.exports = {
  getPlainArtists: getPlainArtists,
  getPlainArtistsByName: getPlainArtistsByName,
  insertArtist: insertArtist,
  deleteArtist: deleteArtist,
  updateArtist: updateArtist
};