var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');

var routes = require('./routes/index');
var songs = require('./routes/songs');
var artists = require('./routes/artists');
var albums = require('./routes/albums');
var genres = require('./routes/genres');
var record_types = require('./routes/record_types');
var sql_queries = require('./routes/sql_queries');

/*
 global base path
 usage (in any file in app):
 var Article = require(__base + 'app/models/article');
 */
global.__base = __dirname + '/';

// db operations
var db = require('./db');
//db.initDb();  // TODO: set by environment variable

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'hbs');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', routes);
app.use('/songs', songs);
app.use('/artists', artists);
app.use('/albums', albums);
app.use('/genres', genres);
app.use('/record_types', record_types);
app.use('/sql_queries', sql_queries);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function (err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: err
    });
  });
}

// production error handler
// no stacktraces leaked to user
app.use(function (err, req, res, next) {
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: {}
  });
});


module.exports = app;
