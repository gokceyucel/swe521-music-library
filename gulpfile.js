/**
 * Created by gkc on 15/05/16.
 */

var gulp = require('gulp');
var nodemon = require('gulp-nodemon');
var electron = require('gulp-electron');
var packageJson = require('./package.json');

gulp.task('start', function () {
  nodemon({
    script: 'bin/www',
    ext: 'js html',
    env: {'NODE_ENV': 'development'}
  });
});

gulp.task('electron', function () {

  gulp.src("")
    .pipe(electron({
      src: './electron/src',
      packageJson: packageJson,
      release: './release',
      cache: './electron/cache',
      version: 'v1.2.1',
      packaging: true,
      token: 'da809a6077bb1b0aa7c5623f7b2d5f1fec2faae4',
      platforms: ['darwin-x64'],
      platformResources: {
        darwin: {
          CFBundleDisplayName: packageJson.name,
          CFBundleIdentifier: packageJson.name,
          CFBundleName: packageJson.name,
          CFBundleVersion: packageJson.version,
          icon: 'gulp-electron.icns'
        }
        //win: {
        //  "version-string": packageJson.version,
        //  "file-version": packageJson.version,
        //  "product-version": packageJson.version,
        //  "icon": 'gulp-electron.ico'
        //}
      }
    }))
    .pipe(gulp.dest(""));
});

gulp.task('electron-packager', function () {
  var packager = require('electron-packager');

  packager({
    dir: '.',
    name: 'MuLi',
    platform: 'darwin',
    arch: 'ia32',
    version: '1.2.1'
  }, function(error, appPath) {
    if (error) {
      throw error;
    }

    console.log(appPath);
  });
});

gulp.task('default', ['start'], function () {
  // place code for your default task here
});
