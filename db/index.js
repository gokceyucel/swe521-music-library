/**
 * Created by gkc on 08/05/16.
 */

var dblite = require('dblite');
var dbPath = 'music_library.db';
var db = dblite(dbPath,'-header');
var fs = require('fs');
var async = require('async');

var createDbFromSchema = function () {

  console.log('creating db schema');

  fs.readdir(__base + 'db/schema', function (err, files) {

    if (err) return console.error(err);

    async.eachSeries(
      files,
      function (file, next) {

        console.log('executing db operations in file ' + file);

        fs.readFile(__base + 'db/schema/' + file, 'utf8', function (err, contents) {
          if (err) return console.error(err);

          // filter empty values caused by by ';' delimiter
          var commands = contents
            .split(';')
            .filter(function (t) {
              return t.trim();
            });

          for (var i = 0; i < commands.length; i++) {

            db.serialize(function () {
              var createTableQuery = commands[i].trim();
              console.log(createTableQuery + '\n');
              db.query(createTableQuery);
            });

          }
          next();
        });
      },
      function (err) {
        if (err) return console.error(err);
        console.log('all db operations done');
        db.close();
      }
    );
  });
};

var initDb = function () {
  createDbFromSchema();
};

module.exports = {
  initDb: initDb,
  db : function () {
    //return new sqlite3.Database(dbPath);
    return db;
  }
};