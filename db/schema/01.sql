CREATE TABLE albums (
    id INTEGER PRIMARY KEY,
    artist_id INTEGER NOT NULL,
    name VARCHAR NOT NULL,
    year INTEGER NOT NULL,
    record_type_id INTEGER NOT NULL
);

CREATE UNIQUE INDEX idx_album_id ON albums (id);

CREATE TABLE artists (
    id INTEGER PRIMARY KEY,
    name VARCHAR NOT NULL,
    bio VARCHAR
);

CREATE UNIQUE INDEX idx_artist_id ON artists (id);

CREATE TABLE songs (
    id INTEGER PRIMARY KEY,
    name VARCHAR NOT NULL,
    play_time TIME NOT NULL,
    artist_id INTEGER NOT NULL,
    album_id INTEGER,
    genre_id INTEGER NOT NULL,
    year INTEGER
);

CREATE UNIQUE INDEX idx_song_id ON songs (id);

CREATE TABLE genres (
    id INTEGER PRIMARY KEY,
    name VARCHAR NOT NULL
);

CREATE UNIQUE INDEX idx_genre_id ON genres (id);

CREATE TABLE record_types (
    id INTEGER PRIMARY KEY,
    name VARCHAR NOT NULL
);

CREATE UNIQUE INDEX idx_record_type_id ON record_types (id);

CREATE TABLE album_record_types (
    album_id INTEGER NOT NULL,
    record_type_id INTEGER NOT NULL
);

CREATE UNIQUE INDEX idx_album_id_record_type_id ON album_record_types (album_id, record_type_id);
