
## SWE 521 Database Systems Project June 2016



#### What is this?
A Music Library desktop application project which provides users to store and manage their albums in a digital environment. There are insertion, deletion and modification abilities for organisation purposes.


#### Technology
This application is built on top of the well-known Node.js platform and packaged using Electron framework for 64bit Windows Operating System environment. 


#### How it works?
Upon running, a minimal Node.js web server starts in background listening on port 3000. Note that this port should be allowed if there is a firewall in the operating system. By default, user will be prompted for this permission. Presentation layer communicates with the server via http requests.


#### Dependencies
Application depends on a minimal sqlite database which should be available on operating system’s path (i.e. C:\Windows\System32). Precompiled Windows Binaries can be found at https://www.sqlite.org/download.html. Note that an active internet connection may be required due to some front-end dependencies such as icons, fonts and images. These are provided by third-parties over CDN.


#### Data Structure
If there is no implicit database exists in the application, it will be created automatically upon application’s first run using the sql scripts provided at the end of this document. However, a pre-defined database is already embedded to simplify the usage.


#### Additional Info
This is an open-source project and the source code can be found at https://bitbucket.org/gokceyucel/swe521-music-library















#### Sql Scripts
```sql
CREATE TABLE albums (
    id INTEGER PRIMARY KEY,
    artist_id INTEGER NOT NULL,
    name VARCHAR NOT NULL,
    year INTEGER NOT NULL,
    record_type_id INTEGER NOT NULL
);
CREATE UNIQUE INDEX idx_album_id ON albums (id);

CREATE TABLE artists (
    id INTEGER PRIMARY KEY,
    name VARCHAR NOT NULL,
    bio VARCHAR
);
CREATE UNIQUE INDEX idx_artist_id ON artists (id);

CREATE TABLE songs (
    id INTEGER PRIMARY KEY,
    name VARCHAR NOT NULL,
    play_time TIME NOT NULL,
    artist_id INTEGER NOT NULL,
    album_id INTEGER,
    genre_id INTEGER NOT NULL,
    year INTEGER
);
CREATE UNIQUE INDEX idx_song_id ON songs (id);

CREATE TABLE genres (
    id INTEGER PRIMARY KEY,
    name VARCHAR NOT NULL
);
CREATE UNIQUE INDEX idx_genre_id ON genres (id);

CREATE TABLE record_types (
    id INTEGER PRIMARY KEY,
    name VARCHAR NOT NULL
);
CREATE UNIQUE INDEX idx_record_type_id ON record_types (id);

CREATE TABLE album_record_types (
    album_id INTEGER NOT NULL,
    record_type_id INTEGER NOT NULL
);
CREATE UNIQUE INDEX idx_album_id_record_type_id ON album_record_types (album_id, record_type_id);
```

